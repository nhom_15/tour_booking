package iuh.feedbackservice.service;

import iuh.feedbackservice.entity.Feedback;
import iuh.feedbackservice.repository.FeedbackRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;


@Service
public class FeedbackService {

    @Autowired
    private FeedbackRepository feedbackRepository;

    public Feedback createFeedBack(Feedback tour) {
        return feedbackRepository.save(tour);
    }

    public List<Feedback> getAllFeedBacks() {
        return feedbackRepository.findAll();
    }

    public Feedback updateFeedback(Long id, Feedback feedbackDetails) {
        Feedback feedback = feedbackRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "feedback not found with id " + id));
        feedback.setComment(feedbackDetails.getComment());
        return feedbackRepository.save(feedback);
    }

    public void deleteFeedback(Long id) {
        Feedback feedback = feedbackRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Tour not found with id " + id));
        feedbackRepository.delete(feedback);
    }
}
