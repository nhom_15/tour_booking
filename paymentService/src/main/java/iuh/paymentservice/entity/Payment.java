package iuh.paymentservice.entity;
import lombok.*;

import jakarta.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
@Table
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Payment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private double amount;
    private String paymentMethod;
    private String status;
}