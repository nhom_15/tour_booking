# Nhóm 15
# Thành viên:
Nguyễn Thanh Tuấn: 20040861 <br>
Nguyễn Hữu Huy: 20102781 <br>
# Project: Tour manager mircoservice
![MicroSerivceProject15 drawio](https://github.com/HuyGlaucus/KTPM_Lab05/assets/116423850/56bbc2fb-f021-483f-b0d7-d6929b37a831)
#Docker all service
![image](https://github.com/user-attachments/assets/c3faec85-b6a6-446d-a277-68e2734076f0)
![image](https://github.com/user-attachments/assets/cc857a92-b969-4576-a244-76ff9c02bc8f)
#JWT
![image](https://github.com/user-attachments/assets/b188a147-aedb-477d-b608-bb8df1696192)
![image](https://github.com/user-attachments/assets/fc809e09-c984-4e7c-bd43-6d324d122278)
#Redis
![image](https://github.com/user-attachments/assets/d85f02e2-a38a-4988-927a-c3c0a1c02c05)
#Retry 5s
![image](https://github.com/user-attachments/assets/0f9a27e4-db67-47be-bc0a-1c042ddedd23)
#CircuitBreaker
![image](https://github.com/user-attachments/assets/4d0a35a7-bbe6-4d90-af5e-46a590742162)
#Rate Limiter
![image](https://github.com/user-attachments/assets/656672fd-2f40-42df-9597-d0008dd11c45)
![image](https://github.com/user-attachments/assets/e9e31fbb-7a46-431c-aec3-b8ea9bc81900)
#Gitlab CI/CD
![image](https://github.com/user-attachments/assets/9b188e39-b4c9-4956-ac00-879837e0a1a5)