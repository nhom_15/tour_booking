package iuh.analysisservice.controllers;


import iuh.analysisservice.entity.Payment;
import iuh.analysisservice.entity.Tour;
import iuh.analysisservice.service.AnalysisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@RestController
@RequestMapping("/api/v1/analyses")
public class AnalysisController {
    @Autowired
    private AnalysisService analysisService;

    @Autowired
    private RestTemplate restTemplate;

    @GetMapping("/tours")
    public ResponseEntity<List<Tour>> getAllTours() {
        List<Tour> tours = analysisService.getAllTours();
        return new ResponseEntity<>(tours, HttpStatus.OK);
    }

    @GetMapping("/payments")
    public ResponseEntity<List<Payment>> getAllPayments() {
        List<Payment> payments = analysisService.getAllPayments();
        return new ResponseEntity<>(payments, HttpStatus.OK);
    }

}
