package iuh.analysisservice.repository;

import iuh.analysisservice.entity.Analysis;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AnalysisRepository extends JpaRepository<Analysis, Long>{
}
