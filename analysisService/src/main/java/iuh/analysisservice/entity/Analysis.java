package iuh.analysisservice.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import jakarta.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
@Table
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Analysis {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long tourId;
    private Long totalBookings;
    private BigDecimal totalRevenue;
    private Long totalFeedbacks;
    private Date analysisDate;
}