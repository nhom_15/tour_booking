package iuh.analysisservice.service;

import iuh.analysisservice.entity.Payment;
import iuh.analysisservice.entity.Tour;
import iuh.analysisservice.repository.AnalysisRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Slf4j
@Service
public class AnalysisService {
    @Autowired
    private AnalysisRepository analysisRepository;

    @Autowired
    private RestTemplate restTemplate;

    public List<Tour> getAllTours() {
        ResponseEntity<List<Tour>> response = this.restTemplate.exchange(
                "http://localhost:8806/api/v1/tours/allTours",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<Tour>>() {});
        return response.getBody();
    }

    public List<Payment> getAllPayments() {
        ResponseEntity<List<Payment>> response = this.restTemplate.exchange(
                "http://localhost:8803/api/v1/payments/allPayments",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<Payment>>() {});
        return response.getBody();
    }
}
