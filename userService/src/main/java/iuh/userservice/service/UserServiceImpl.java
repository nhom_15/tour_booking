package iuh.userservice.service;

import iuh.userservice.authen.UserPrincipal;
import iuh.userservice.entity.Tour;
import iuh.userservice.entity.User;
import iuh.userservice.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class UserServiceImpl implements UserService {

    private static final String USER_KEY_PREFIX = "userID: ";

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    @Autowired
    private RestTemplate restTemplate;


    @Override
    public User createUser(User user) {
        User savedUser= userRepository.saveAndFlush(user);
        //lưu user vào redis
        saveUserToRedis(savedUser);
        return savedUser;    }

    @Override
    public UserPrincipal findByUsername(String username) {
        User user = userRepository.findByUsername(username);
        UserPrincipal userPrincipal = new UserPrincipal();

        if (null != user) {

            Set<String> authorities = new HashSet<>();

            if (null != user.getRoles())

                user.getRoles().forEach(r -> {
                authorities.add(r.getRoleKey());
                r.getPermissions().forEach(
                        p -> authorities.add(p.getPermissionKey()));
            });

            userPrincipal.setUserId(user.getId());
            userPrincipal.setUsername(user.getUsername());
            userPrincipal.setPassword(user.getPassword());
            userPrincipal.setAuthorities(authorities);

        }

        return userPrincipal;

    }

    public List<Tour> getAllTours() {
        ResponseEntity<List<Tour>> response = this.restTemplate.exchange(
                "http://localhost:8806/api/v1/tours/allTours",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<Tour>>() {});
        return response.getBody();
    }

    @Override
    public Optional<User> findById(Long id) {
        return userRepository.findById(id);
    }

    @Override
    public void saveUserToRedis(User user) {
        String key = USER_KEY_PREFIX + user.getId();
        String userData = "username:" + user.getUsername() + ",password:" + user.getPassword();
        redisTemplate.opsForValue().set(key, userData);
    }


    @Override
    public User getUserFromRedis(Long id) {
        String key = USER_KEY_PREFIX + id;
        String userData = (String) redisTemplate.opsForValue().get(key);

        if (userData != null && userData.contains(",")) {
            String[] parts = userData.split(",");
            String username = parts[0].split(":")[1].trim();
            String password = parts[1].split(":")[1].trim();

            User user = new User();
            user.setId(id);
            user.setUsername(username);
            user.setPassword(password);

            return user;
        }

        return null;
    }
    @Override
    public void updateUserInRedis(User user) {
        String key = USER_KEY_PREFIX + user.getId();
        String userData = "username:" + user.getUsername() + ",password:" + user.getPassword();
        redisTemplate.opsForValue().set(key, userData);
    }

    @Override
    public void deleteAllUsersFromRedis() {
        // Xóa tất cả dữ liệu trong Redis
        redisTemplate.getConnectionFactory().getConnection().flushAll();
    }
}