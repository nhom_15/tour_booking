package iuh.userservice.service;

import iuh.userservice.authen.UserPrincipal;
import iuh.userservice.entity.Tour;
import iuh.userservice.entity.User;

import java.util.List;
import java.util.Optional;

public interface UserService {
    User createUser(User user);
    UserPrincipal findByUsername(String username);
    void saveUserToRedis(User user);
    void deleteAllUsersFromRedis();
    Optional<User> findById(Long id);
    List<Tour> getAllTours();
    User getUserFromRedis(Long id);
    void updateUserInRedis(User user);
}
