package iuh.userservice.service;

import iuh.userservice.entity.Token;

public interface TokenService {
    Token createToken(Token token);

    Token findByToken(String token);

}
