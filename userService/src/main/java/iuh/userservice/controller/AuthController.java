package iuh.userservice.controller;

import io.github.resilience4j.ratelimiter.annotation.RateLimiter;
import iuh.userservice.authen.UserPrincipal;
import iuh.userservice.entity.Token;
import iuh.userservice.entity.Tour;
import iuh.userservice.entity.User;
import iuh.userservice.service.TokenService;
import iuh.userservice.service.UserService;
import iuh.userservice.util.JwtUtil;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;


@RestController
@RequestMapping("/auth")
public class AuthController {

    @Autowired
    private UserService userService;

    @Autowired
    private JwtUtil jwtUtil;

    @Autowired
    private TokenService tokenService;

    private static final String SERVICE_USER = "serviceUser";

    @PostMapping("/register")
    public User register(@RequestBody User user){
        user.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));

        return userService.createUser(user);
    }

    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody User user, HttpSession session){

        UserPrincipal userPrincipal =
                userService.findByUsername(user.getUsername());

        if (null == user || !new BCryptPasswordEncoder()
                .matches(user.getPassword(), userPrincipal.getPassword())) {

            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body("Account or password is not valid!");
        }

        Token token = new Token();
        token.setToken(jwtUtil.generateToken(userPrincipal));
        token.setTokenExpDate(jwtUtil.generateExpirationDate());
        token.setCreatedBy(userPrincipal.getUserId());
        tokenService.createToken(token);

        session.setAttribute("token", token.getToken());

        return ResponseEntity.ok(token.getToken());
    }
    @GetMapping("/tours")
    @RateLimiter(name = SERVICE_USER)
    public ResponseEntity<List<Tour>> getAllTours() {
        List<Tour> tours = userService.getAllTours();
        return new ResponseEntity<>(tours, HttpStatus.OK);
    }
    //đọc user
    @GetMapping("/users/{id}")
    public ResponseEntity<?> getUserById(@PathVariable Long id) {
        // Kiểm tra trong Redis trước
        User user = userService.getUserFromRedis(id);
        if (user != null) {
            // Nếu có trong Redis, trả về thông tin người dùng từ Redis
            Map<String, Object> response = new HashMap<>();
            response.put("id", user.getId());
            response.put("username", user.getUsername());
            return ResponseEntity.ok(response);
        }

//        // Nếu không có trong Redis, tìm trong cơ sở dữ liệu
//        Optional<User> userOpt = userService.findById(id);
//        if (!userOpt.isPresent()) {
//            return ResponseEntity.status(HttpStatus.NOT_FOUND)
//                    .body("User not found!");
//        }
//
//        user = userOpt.get();
//        Map<String, Object> response = new HashMap<>();
//        response.put("id", user.getId());
//        response.put("username", user.getUsername());
//        return ResponseEntity.ok(response);
        // Nếu không có trong Redis, trả về lỗi hoặc null
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body("User not found in Redis!");
    }
    //update
    @PutMapping("/usersupdate/{id}")
    public ResponseEntity<?> updateUser(@PathVariable Long id, @RequestBody User updatedUser) {
        Optional<User> existingUserOpt = userService.findById(id);
        if (!existingUserOpt.isPresent()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("User not found!");
        }

        User existingUser = existingUserOpt.get();
        existingUser.setUsername(updatedUser.getUsername());
        existingUser.setPassword(new BCryptPasswordEncoder().encode(updatedUser.getPassword()));

        // Lưu user đã cập nhật vào cơ sở dữ liệu (nếu cần)
        userService.createUser(existingUser);

        // Cập nhật user trong Redis
        userService.updateUserInRedis(existingUser);

        return ResponseEntity.ok("User updated successfully!");
    }

    // xóa all
    @PostMapping("/users/deleteAllRedis")
    public ResponseEntity<String> deleteAllUsersFromRedis() {
        userService.deleteAllUsersFromRedis();
        return ResponseEntity.ok("All users deleted from Redis.");
    }










}
