package iuh.tourservice.service;

import iuh.tourservice.entity.Tour;
import iuh.tourservice.repository.TourRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.http.HttpStatus;

import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class TourService {

    @Autowired
    private TourRepository tourRepository;

    public Tour createTour(Tour tour) {
        return tourRepository.save(tour);
    }

    public List<Tour> getAllTours() {
        return tourRepository.findAll();
    }

    public Tour bookTour(Long id, Tour tourDetails) {
        Tour tour = tourRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Tour not found with id " + id));
        tour.setName(tourDetails.getName());
        tour.setDescription(tourDetails.getDescription());
        tour.setLocation(tourDetails.getLocation());
        tour.setPrice(tourDetails.getPrice());
        // Add any additional fields related to booking here
        return tourRepository.save(tour);
    }

    public Optional<Tour> getTourById(Long id) {
        return tourRepository.findById(id);
    }

    public Tour updateTour(Long id, Tour tourDetails) {
        Tour tour = tourRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Tour not found with id " + id));
        tour.setName(tourDetails.getName());
        tour.setDescription(tourDetails.getDescription());
        tour.setLocation(tourDetails.getLocation());
        tour.setPrice(tourDetails.getPrice());
        return tourRepository.save(tour);
    }

    public void deleteTour(Long id) {
        Tour tour = tourRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Tour not found with id " + id));
        tourRepository.delete(tour);
    }
}
