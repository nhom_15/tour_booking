package iuh.bookingservice.service;


import io.github.resilience4j.retry.annotation.Retry;
import iuh.bookingservice.entity.Tour;
import iuh.bookingservice.repository.BookingRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Slf4j
@Service
public class BookingService {
    @Autowired
    private BookingRepository bookingRepository;

    @Autowired
    private RestTemplate restTemplate;

    @Retry(name = "retryApi")
    public List<Tour> getAllTours() {
        ResponseEntity<List<Tour>> response = this.restTemplate.exchange(
                "http://localhost:8086/api/v1/tours/allTours",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<Tour>>() {});
        return response.getBody();
    }



}