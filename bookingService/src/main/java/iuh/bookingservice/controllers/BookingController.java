package iuh.bookingservice.controllers;

import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.ratelimiter.annotation.RateLimiter;
import iuh.bookingservice.entity.Booking;
import iuh.bookingservice.entity.Tour;
import iuh.bookingservice.entity.User;
import iuh.bookingservice.service.BookingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
        import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("/api/v1/bookings")
public class BookingController {

    @Autowired
    private BookingService bookingService;
    @Autowired
    private RestTemplate restTemplate;

    private static final String SERVICE_BOOKING = "serviceBooking";

    @GetMapping("/tours")
    @CircuitBreaker(name = SERVICE_BOOKING, fallbackMethod = "serviceBookingFallback")
    public ResponseEntity<List<Tour>> getAllTours() {
        List<Tour> tours = bookingService.getAllTours();
        return new ResponseEntity<>(tours, HttpStatus.OK);
    }

    public ResponseEntity<List<Tour>> serviceBookingFallback(final Throwable throwable) {
        return new ResponseEntity<>(Collections.emptyList(), HttpStatus.SERVICE_UNAVAILABLE);
    }
}